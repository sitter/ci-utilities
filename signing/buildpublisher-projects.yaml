# SPDX-License-Identifier: CC0-1.0
# SPDX-FileCopyrightText: 2023 Ben Cooksley <bcooksley@kde.org>

# define defaults for all listed projects
defaults:
  # template used to construct the destination that rsync should be syncing to
  rsyncDestination: "cibuilds@tinami.kde.org:/srv/www/cdn.kde.org/ci-builds/{projectPath}/{branch}/{platform}/"
  # require all branches to be explicitly permitted
  branches: []

##########################################################################################################
# The entries are sorted by their project name (i.e. the name of the directory for the project on invent)
##########################################################################################################

office/alkimia:
  branches:
    master:

network/alligator:
  branches:
    master:

utilities/ark:
  branches:
    release/24.08:
    release/24.12:

education/blinken:
  branches:
    master:

games/bomber:
  branches:
    master:

games/bovo:
  branches:
    master:

office/calligra:
  branches:
    master:

office/calligraplan:
  branches:
    master:
    3.3:

education/cantor:
  branches:
    master:
    release/24.08:
    release/24.12:

office/crow-translate:
  branches:
    master:
    release/3.0:

graphics/digikam:
  branches:
    master:

graphics/krita:
  branches:
    master:
    krita/5.2:

system/dolphin:
  branches:
    master:
    release/24.08:
    release/24.12:

multimedia/elisa:
  branches:
    master:
    release/24.08:
    release/24.12:

utilities/filelight:
  branches:
    master:
    release/24.08:
    release/24.12:

graphics/glaxnimate:
  branches:
    master:

games/granatier:
  branches:
    master:

multimedia/haruna:
  branches:
    master:

utilities/isoimagewriter:
  branches:
    master:

network/kaidan:
  branches:
    master:

education/kalgebra:
  branches:
    master:

games/kapman:
  branches:
    master:

multimedia/kasts:
  branches:
    master:
    release/24.08:
    release/24.12:

utilities/kate:
  branches:
    master:
    release/24.08:
    release/24.12:

games/katomic:
  branches:
    master:

office/kbibtex:
  branches:
    master:

games/kblackbox:
  branches:
    master:

games/kblocks:
  branches:
    master:

education/kbruch:
  branches:
    master:

games/kbounce:
  branches:
    master:

games/kbreakout:
  branches:
    master:

multimedia/kdenlive:
  branches:
    master:
    release/24.08:
    release/24.12:

network/kdeconnect-kde:
  branches:
    master:
    release/24.08:
    release/24.12:

kdevelop/kdevelop:
  branches:
    master:
    release/24.08:
    release/24.12:

games/kdiamond:
  branches:
    master:

sdk/kdiff3:
  branches:
    master:
    1.11:

office/kexi:
  branches:
    master:
    3.2:

games/kfourinline:
  branches:
    master:

education/kgeography:
  branches:
    master:

games/kgoldrunner:
  branches:
    master:

multimedia/kid3:
  branches:
    master:

education/kig:
  branches:
    master:

games/kigo:
  branches:
    master:

office/kile:
  branches:
    master:

games/killbots:
  branches:
    master:

sdk/kirigami-gallery:
  branches:
    master:

games/kiriki:
  branches:
    master:

education/kiten:
  branches:
    master:
    release/24.08:
    release/24.12:

games/kjumpingcube:
  branches:
    master:

games/klickety:
  branches:
    master:

games/klines:
  branches:
    master:

games/kmahjongg:
  branches:
    master:

games/kmines:
  branches:
    master:

education/kmplot:
  branches:
    master:

office/kmymoney:
  branches:
    master:
    5.1:

games/knavalbattle:
  branches:
    master:

games/knetwalk:
  branches:
    master:

games/kolf:
  branches:
    master:

games/kollision:
  branches:
    master:

graphics/kolourpaint:
  branches:
    master:

utilities/konsole:
  branches:
    master:

games/konquest:
  branches:
    master:

network/konversation:
  branches:
    master:
    release/24.08:
    release/24.12:

games/kpat:
  branches:
    master:

games/kreversi:
  branches:
    master:

utilities/kronometer:
  branches:
    master:

games/kshisen:
  branches:
    master:

games/ksnakeduel:
  branches:
    master:

games/kspaceduel:
  branches:
    master:

games/ksquares:
  branches:
    master:

education/kstars:
  branches:
    master:
    stable-3.7.2:
    stable-3.7.3:

games/ksudoku:
  branches:
    master:

system/ksystemlog:
  branches:
    master:

pim/ktimetracker:
  branches:
    master:

utilities/ktrip:
  branches:
    master:
    release/24.08:
    release/24.12:

games/ktuberling:
  branches:
    master:

education/kturtle:
  branches:
    master:

games/kubrick:
  branches:
    master:

education/kwordquiz:
  branches:
    master:

education/labplot:
  branches:
    master:
    release/2.10:

sdk/lokalize:
  branches:
    master:
    release/24.08:
    release/24.12:

games/lskat:
  branches:
    master:

education/marble:
  branches:
    master:

office/marknote:
  branches:
    master:

pim/merkuro:
  branches:
    master:

education/minuet:
  branches:
    master:

network/neochat:
  branches:
    master:
    release/24.08:
    release/24.12:

utilities/okteta:
  branches:
    master:

graphics/okular:
  branches:
    master:
    release/24.08:
    release/24.12:

education/parley:
  branches:
    master:

graphics/peruse:
  branches:
    master:

games/picmi:
  branches:
    master:

education/rkward:
  branches:
    master:
    kf5:
    releases/0.8.0:

network/ruqola:
  branches:
    master:
    2.1:

libraries/snoretoast:
  branches:
    master:

office/skrooge:
  branches:
    master:

education/step:
  branches:
    master:
    release/24.08:
    release/24.12:

network/tokodon:
  branches:
    master:
    release/24.08:
    release/24.12:

plasma-mobile/calindori:
  branches:
    master:

sdk/umbrello:
  branches:
    master:
    release/24.08:
    release/24.12:

utilities/klimbgrades:
  branches:
    master:

utilities/kongress:
  branches:
    release/24.08:
    release/24.12:

accessibility/kontrast:
  branches:
    master:

libraries/kosmindoormap:
  branches:
    master:

utilities/krecorder:
  branches:
    master:

utilities/kweather:
  branches:
    master:

plasma-mobile/plasma-camera:
  branches:
    master:

plasma-mobile/plasma-phonebook:
  branches:
    master:

utilities/qrca:
  branches:
    master:

utilities/toad:
  branches:
    master:

pim/vakzination:
  branches:
    master:

pim/itinerary:
  branches:
    release/24.08:
    release/24.12:

neon/ubuntu-core:
  branches:
    master:
    Neon/core22:
